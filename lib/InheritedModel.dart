// Inherited Widgets

// ignore_for_file: file_names

import 'package:flutter/material.dart';

//the parent widget
class MyAncestor extends InheritedWidget {
  const MyAncestor(this.colorOne, this.colorTwo, Widget child)
      : super(child: child);

  final Color colorOne;
  final Color colorTwo;

  @override
  bool updateShouldNotify(MyAncestor oldWidget) {
    return colorOne != oldWidget.colorOne || colorTwo != oldWidget.colorTwo;
  }
}

class ColorOneWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ancestor = context.dependOnInheritedWidgetOfExactType<MyAncestor>();

    return Container(
      color: ancestor!.colorOne,
    );
  }
}

class ColorTwoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ancestor = context.dependOnInheritedWidgetOfExactType<MyAncestor>();

    return Container(
      color: ancestor!.colorTwo,
    );
  }
}

// Inherited Models

//the parent widget
class MyAncestor2 extends InheritedModel<String> {
  const MyAncestor2(this.colorOne, this.colorTwo, Widget child)
      : super(child: child);

  final Color colorOne;
  final Color colorTwo;

  @override
  bool updateShouldNotify(MyAncestor2 oldWidget) {
    return colorOne != oldWidget.colorOne || colorTwo != oldWidget.colorTwo;
  }

  @override
  bool updateShouldNotifyDependent(
      MyAncestor2 oldWidget, Set<String> dependencies) {
    if (dependencies.contains('one') && colorOne != oldWidget.colorOne) {
      return true;
    }
    if (dependencies.contains('two') && colorTwo != oldWidget.colorTwo) {
      return true;
    }
    return false;
  }
}

class ColorOneWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ancestor =
        InheritedModel.inheritFrom<MyAncestor2>(context, aspect: 'one');

    return Container(
      color: ancestor!.colorOne,
    );
  }
}

class ColorTwoWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ancestor =
        InheritedModel.inheritFrom<MyAncestor2>(context, aspect: 'two');

    return Container(
      color: ancestor!.colorTwo,
    );
  }
}
