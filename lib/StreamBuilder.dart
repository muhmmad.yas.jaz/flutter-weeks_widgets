// ignore_for_file: file_names

import 'package:flutter/material.dart';

class StreamBuilderTutorial extends StatefulWidget {
  const StreamBuilderTutorial({Key? key}) : super(key: key);

  @override
  State<StreamBuilderTutorial> createState() => _StreamBuilderState();
}

Stream<int> count() async* {
  int i = 1;
  while (true) {
    yield i++;
  }
}

class _StreamBuilderState extends State<StreamBuilderTutorial> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: count(),
        initialData: 42,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return UhOh(snapshot.error);
          } else {
            return MyWidget(snapshot.data);
          } 

          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return const LinearProgressIndicator();
            case ConnectionState.active:
              return MyWidget(snapshot.data);
            case ConnectionState.done:
              return MyFinalWidget(snapshot.data);
          }

        });
  }
}

Widget MyWidget(data) {
  return Container(
    color: Colors.black,
  );
}

Widget MyFinalWidget(data) {
  return Container(
    color: Colors.yellow,
  );
}

Widget UhOh(data) {
  return Text(data, style: const TextStyle(color: Colors.red));
}
