// ignore_for_file: file_names

import 'package:flutter/material.dart';

class FadeInImageTutorial extends StatefulWidget {
  const FadeInImageTutorial({Key? key}) : super(key: key);

  @override
  State<FadeInImageTutorial> createState() => _FadeInImageState();
}

class _FadeInImageState extends State<FadeInImageTutorial> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: FadeInImage.assetNetwork(
              height: 100.0,
              width: 50.0,
              fadeInDuration: const Duration(seconds: 1),
              fadeInCurve: Curves.bounceIn,
              placeholder: 'assets/images/loading.gif',
              image: 'https://images.indianexpress.com/2019/07/baby1.jpeg')),

      // for transparent image: import 'package:transparent_image....
      // then use the memoory newtwork function in FadeInImage

      //the memory network:
      // FadeInImage.memoryNetwork(placeholder: 'kTransparentImage', image: 'image')
    );
  }
}
